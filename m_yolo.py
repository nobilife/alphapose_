import logging
import subprocess
import time
import os
import cv2
from uuid import uuid4
from cv2 import data
import pandas as pd
from tqdm import tqdm
import re


class YoloV4():
    def __init__(self, darknet: str, config_file: str, data_file: str, weights_file: str):
        # Use the path to the darknet file I sent you
        self.darknet = darknet
        self.config_file = config_file
        self.data_file = data_file
        self.weights_file = weights_file
        self.first_time = True

        assert os.path.exists(self.config_file)
        assert os.path.exists(self.weights_file)
        self.pr = subprocess.Popen(
            [self.darknet, 'detector', 'test', self.data_file, self.config_file, self.weights_file, '-dont_show', '-ext_output'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, universal_newlines=True)
        print(self.pr)

    def stop(self):
        self.pr.stdin.close()

    def process(self, camera_images, _ceil=True):
        detected_objects = []
        for cam_idx, camera_image in enumerate(camera_images):
            detections = []
            image_path = self.__preprocess(camera_image)
            output = ""
            self.pr.stdin.write(image_path + "\n")
            while True:
                c = self.pr.stdout.read(1)
                output += c
                if "Enter Image Path" in output:
                    if self.first_time:
                        output = ""
                        self.first_time = False
                        continue
                    break
            result = output.split("\n")
            for i in range(5, len(result)):
                data = result[i-1]
                label = data.split(":")[0]
                confidient = int(data.split(":")[1].split("%")[0])
                bbox = data.split("(")[1].split(")")[0]
                if _ceil:
                    left_x = int(re.search('left_x:(.*)top_y', bbox).group(1))
                    left_x = 0 if left_x < 0 else left_x
                    top_y = int(re.search('top_y:(.*)width', bbox).group(1))
                    top_y = 0 if top_y < 0 else top_y
                    width = int(re.search('width:(.*)height', bbox).group(1))
                    height = int(re.search('height:(.*)', bbox).group(1))
                else:
                    left_x = float(re.search('left_x:(.*)top_y', bbox).group(1))
                    left_x = 0. if left_x < 0 else left_x
                    top_y = float(re.search('top_y:(.*)width', bbox).group(1))
                    top_y = 0. if top_y < 0 else top_y
                    width = float(re.search('width:(.*)height', bbox).group(1))
                    height = float(re.search('height:(.*)', bbox).group(1))

                detections.append(
                    (label, confidient, (left_x, top_y, width, height), cam_idx))

            detected_objects = detected_objects + \
                self.__postprocess(camera_image, detections)
            os.remove(image_path)
        return detected_objects

    def __preprocess(self, camera_image):
        path = os.getcwd()
        image_path = path + "/" + uuid4().hex + ".jpg"
        cv2.imwrite(image_path, camera_image)
        return image_path

    def __postprocess(self, camera_image, detections):
        detected_objects = []
        for detection in detections:
            label, confidence, (x, y, w, h), cam_id = detection
            detected_objects.append([
                camera_image,       # CameraImage
                label,              # str
                (x, y, w, h),       # BoundingBox
                confidence,         # int
                cam_id
            ])
        return detected_objects

if __name__ == "__main__":
    yolo = YoloV4(darknet="/mnt/1882C07482C05840/TensorRT/model-zoo/darknet/darknet",
                    config_file="/mnt/1882C07482C05840/TensorRT/model-zoo/yolov4/yolov4-608.cfg",
                    data_file="/mnt/1882C07482C05840/TensorRT/model-zoo/yolov4/yolov4-608.data",
                    weights_file="/mnt/1882C07482C05840/TensorRT/model-zoo/yolov4/yolov4-608.weights")

    
    # yolo.process([image])