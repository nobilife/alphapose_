import torch
from alphapose.models import builder
from alphapose.utils.config import update_config


def export_onnx(model_path, cfg_path, onnx_name=None):
    cfg = update_config(cfg_path)
    device = "cuda" if torch.cuda.is_available() else "cpu"
    pose_model = builder.build_sppe(cfg.MODEL, preset_cfg=cfg.DATA_PRESET)
    pose_model.load_state_dict(torch.load(model_path, map_location=device))
    pose_model.to(device)
    pose_model.eval()

    input_names = ['input']
    output_names = ['output']

    dummy_input = torch.randn(
        1, 3, *cfg.DATA_PRESET.IMAGE_SIZE, dtype=torch.float32).to('cuda:0')
    if onnx_name is None:
        onnx_name = "alphaPose_{}_3_{}_{}_dynamic.onnx".format(
            1, *cfg.DATA_PRESET.IMAGE_SIZE)
    onnx_file_name = onnx_name

    torch.onnx.export(pose_model, dummy_input, onnx_file_name, input_names=input_names,
                      output_names=output_names, verbose=True, opset_version=11)


if __name__ == "__main__":
    export_onnx(model_path="model-zoo/alpha_pose/fast_res50_256x192.pth",
                cfg_path="model-zoo/alpha_pose/256x192_res50_lr1e-3_1x.yaml",
                onnx_name="model-zoo/alpha_pose/fast_res50_256x192.onnx")
    # ./trtexec --onnx=model-zoo/alpha_pose/fast_res50_256x192.onnx --saveEngine=model-zoo/alpha_pose/fast_res50_256x192.engine --workspace=2048 --fp16 --explicitBatch

    export_onnx(model_path="model-zoo/fast_pose_res152/fast_421_res152_256x192.pth",
                cfg_path="model-zoo/fast_pose_res152/256x192_res152_lr1e-3_1x-duc.yaml",
                onnx_name="model-zoo/fast_pose_res152/fast_421_res152_256x192.onnx")
    # ./trtexec --onnx=model-zoo/fast_pose_res152/fast_421_res152_256x192.onnx --saveEngine=model-zoo/fast_pose_res152/fast_421_res152_256x192.engine --workspace=2048 --fp16 --explicitBatch