import argparse
import torch
import os
import platform
import sys
import math
import time

import cv2
import numpy as np

from alphapose.utils.transforms import get_func_heatmap_to_coord
from alphapose.utils.pPose_nms import pose_nms
from alphapose.utils.presets import SimpleTransform
from alphapose.models import builder
from alphapose.utils.config import update_config
from alphapose.utils.vis import vis_frame
import subprocess

import tensorrt as trt
import pycuda.driver as cuda
from functools import reduce
import pycuda.autoinit


def cvt_Yolo_format_to_Alpha(detected_objects):
    boxes = []
    scores = []
    for det_obj in detected_objects:
        camera_image, label, (x, y, w, h), conf, cam_id = det_obj
        boxes.append([x, y, x + w, y + h])
        scores.append([conf/100.])

    return torch.Tensor(boxes).to(dtype=torch.float32), torch.Tensor(scores).to(dtype=torch.float32)


class option():
    tracking = False
    showbox = False


try:
    # Sometimes python2 does not understand FileNotFoundError
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError

# Simple helper data class that's a little nicer to use than a 2-tuple.


class HostDeviceMem(object):
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()

# Allocates all buffers required for an engine, i.e. host/device inputs/outputs.


def allocate_buffers(engine, batch_size):
    inputs = []
    outputs = []
    bindings = []
    stream = cuda.Stream()
    for binding in engine:

        size = trt.volume(engine.get_binding_shape(binding)) * batch_size
        dims = engine.get_binding_shape(binding)

        # in case batch dimension is -1 (dynamic)
        if dims[0] < 0:
            size *= -1

        dtype = trt.nptype(engine.get_binding_dtype(binding))
        # Allocate host and device buffers
        host_mem = cuda.pagelocked_empty(size, dtype)
        device_mem = cuda.mem_alloc(host_mem.nbytes)
        # Append the device buffer to device bindings.
        bindings.append(int(device_mem))
        # Append to the appropriate list.
        if engine.binding_is_input(binding):
            inputs.append(HostDeviceMem(host_mem, device_mem))
        else:
            outputs.append(HostDeviceMem(host_mem, device_mem))
    return inputs, outputs, bindings, stream

# This function is generalized for multiple inputs/outputs.
# inputs and outputs are expected to be lists of HostDeviceMem objects.


def do_inference(context, bindings, inputs, outputs, stream):
    # Transfer input data to the GPU.
    [cuda.memcpy_htod_async(inp.device, inp.host, stream) for inp in inputs]
    # Run inference.
    context.execute_async(bindings=bindings, stream_handle=stream.handle)
    # Transfer predictions back from the GPU.
    [cuda.memcpy_dtoh_async(out.host, out.device, stream) for out in outputs]
    # Synchronize the stream
    stream.synchronize()
    # Return only the host outputs.
    return [out.host for out in outputs]


def get_engine(engine_path):
    # If a serialized engine exists, use it instead of building an engine.
    print("Reading engine from file {}".format(engine_path))
    with open(engine_path, "rb") as f, trt.Runtime(TRT_LOGGER) as runtime:
        return runtime.deserialize_cuda_engine(f.read())


TRT_LOGGER = trt.Logger()


class Model_TRT():
    def __init__(self, engine_path, IN_IMAGE_H, IN_IMAGE_W):
        # engine_path = "al.engine"
        # engine_path = "alphaPose_1_3_256_192_dynamic.engine"
        self.engine = get_engine(engine_path)
        self.context = self.engine.create_execution_context()
        self.IN_IMAGE_H = IN_IMAGE_H
        self.IN_IMAGE_W = IN_IMAGE_W
        self.buffers = allocate_buffers(self.engine, 1)
        # dummy_input = np.random.rand(1, 3, IN_IMAGE_H, IN_IMAGE_W).astype(np.float32)
        self.inputs, self.outputs, self.bindings, self.stream = self.buffers

    def __call__(self, img_in, batchs_size=1):
        self.context.set_binding_shape(
            0, (1, 3, self.IN_IMAGE_H, self.IN_IMAGE_W))
        img_in = np.ascontiguousarray(img_in)
        self.inputs[0].host = img_in
        trt_outputs = do_inference(
            self.context, bindings=self.bindings, inputs=self.inputs, outputs=self.outputs, stream=self.stream)
        y_trt = trt_outputs[0].reshape((1, 17, 64, 48))
        return y_trt


class AlphaPose():
    def __init__(self, engine_path, cfg_path):
        self.cfg = update_config(cfg_path)
        self.pose_model = Model_TRT(engine_path, *self.cfg.DATA_PRESET.IMAGE_SIZE)
        self.opt = option()
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.pose_dataset = builder.retrieve_dataset(self.cfg.DATASET.TRAIN)
        self.vis_frame = vis_frame
        self.eval_joints = list(range(self.cfg.DATA_PRESET.NUM_JOINTS))
        self.heatmap_to_coord = get_func_heatmap_to_coord(self.cfg)

        self.transformation = SimpleTransform(
            self.pose_dataset, scale_factor=0,
            input_size=self.cfg.DATA_PRESET.IMAGE_SIZE,
            output_size=self.cfg.DATA_PRESET.HEATMAP_SIZE,
            rot=0, sigma=self.cfg.DATA_PRESET.SIGMA,
            train=False, add_dpg=False, gpu_device=self.device)

    def inference(self, orig_img, boxes, scores):  # orig_img in BGR format
        orig_img = cv2.cvtColor(orig_img, cv2.COLOR_BGR2RGB)
        ids = torch.zeros(scores.shape)
        print("long")
        import ipdb; ipdb.set_trace
        with torch.no_grad():
            (orig_img, boxes)  # = self.det
            if boxes is None or len(boxes) == 0:
                # return None, orig_img, boxes, scores, ids, None
                result = {
                    'result': []
                }

            cropped_boxes = torch.zeros(boxes.size(0), 4)
            hm = torch.zeros(boxes.size(0), 17, 64, 48)
            for i, box in enumerate(boxes):
                inp, cropped_box = self.transformation.test_transform(
                    orig_img, box)
                cropped_boxes[i] = torch.FloatTensor(cropped_box)

                hm[i] = torch.FloatTensor(self.pose_model(
                    inp.view(1, 3, *self.cfg.DATA_PRESET.IMAGE_SIZE)))

            #import ipdb; ipdb.set_trace()
            # self.pose = (inps, orig_img, boxes, scores, ids, cropped_boxes)
            # inps = inps.to(self.device)

            result = self.postprocess(
                boxes, scores, ids, hm, cropped_boxes, orig_img)
            # return inps, orig_img, boxes, scores, ids, cropped_boxes
            return result

    def postprocess(self, boxes, scores, ids, hm_data, cropped_boxes, orig_img):
        norm_type = self.cfg.LOSS.get('NORM_TYPE', None)
        hm_size = self.cfg.DATA_PRESET.HEATMAP_SIZE

        # get item
        (boxes, scores, ids, hm_data, cropped_boxes, orig_img)  # = self.item
        if orig_img is None:
            return None
        # image channel RGB->BGR
        orig_img = np.array(orig_img, dtype=np.uint8)[:, :, ::-1]
        self.orig_img = orig_img
        if boxes is None or len(boxes) == 0:
            return None
        else:
            # location prediction (n, kp, 2) | score prediction (n, kp, 1)
            assert hm_data.dim() == 4
            if hm_data.size()[1] == 136:
                self.eval_joints = [*range(0, 136)]
            elif hm_data.size()[1] == 26:
                self.eval_joints = [*range(0, 26)]
            pose_coords = []
            pose_scores = []

            for i in range(hm_data.shape[0]):
                bbox = cropped_boxes[i].tolist()
                pose_coord, pose_score = self.heatmap_to_coord(
                    hm_data[i][self.eval_joints], bbox, hm_shape=hm_size, norm_type=norm_type)
                pose_coords.append(torch.from_numpy(pose_coord).unsqueeze(0))
                pose_scores.append(torch.from_numpy(pose_score).unsqueeze(0))
            preds_img = torch.cat(pose_coords)
            preds_scores = torch.cat(pose_scores)

            boxes, scores, ids, preds_img, preds_scores, pick_ids = \
                pose_nms(boxes, scores, ids, preds_img,
                         preds_scores, 0)

            _result = []
            for k in range(len(scores)):
                _result.append(
                    {
                        'keypoints': preds_img[k],
                        'kp_score': preds_scores[k],
                        'proposal_score': torch.mean(preds_scores[k]) + scores[k] + 1.25 * max(preds_scores[k]),
                        'idx': ids[k],
                        'bbox': [boxes[k][0], boxes[k][1], boxes[k][2] - boxes[k][0], boxes[k][3] - boxes[k][1]]
                    }
                )
            result = {
                'result': _result
            }
        return result

    def vis(self, image, pose):
        if pose is not None:
            image = self.vis_frame(image, pose, self.opt)
        return image


class AlphaPose_v0():
    def __init__(self, model_path, cfg_path):
        self.cfg = update_config(cfg_path)
        self.device = "cuda" if torch.cuda.is_available() else "cpu"

        # Load pose model
        self.pose_model = builder.build_sppe(
            self.cfg.MODEL, preset_cfg=self.cfg.DATA_PRESET)

        self.pose_model.load_state_dict(torch.load(
            model_path, map_location=self.device))

        self.pose_model.to(self.device)
        self.pose_model.eval()
        self.opt = option()
        self.pose_dataset = builder.retrieve_dataset(self.cfg.DATASET.TRAIN)
        self.vis_frame = vis_frame
        self.eval_joints = list(range(self.cfg.DATA_PRESET.NUM_JOINTS))
        self.heatmap_to_coord = get_func_heatmap_to_coord(self.cfg)

        self.transformation = SimpleTransform(
            self.pose_dataset, scale_factor=0,
            input_size=self.cfg.DATA_PRESET.IMAGE_SIZE,
            output_size=self.cfg.DATA_PRESET.HEATMAP_SIZE,
            rot=0, sigma=self.cfg.DATA_PRESET.SIGMA,
            train=False, add_dpg=False, gpu_device=self.device)

    def inference(self, orig_img, boxes, scores):  # orig_img in BGR format
        orig_img = cv2.cvtColor(orig_img, cv2.COLOR_BGR2RGB)
        ids = torch.zeros(scores.shape)

        with torch.no_grad():
            (orig_img, boxes)  # = self.det
            if boxes is None or len(boxes) == 0:
                # return None, orig_img, boxes, scores, ids, None
                result = {
                    'result': []
                }

            inps = torch.zeros(boxes.size(0), 3, *
                               self.cfg.DATA_PRESET.IMAGE_SIZE)
            cropped_boxes = torch.zeros(boxes.size(0), 4)

            for i, box in enumerate(boxes):
                inps[i], cropped_box = self.transformation.test_transform(
                    orig_img, box)
                cropped_boxes[i] = torch.FloatTensor(cropped_box)

            # self.pose = (inps, orig_img, boxes, scores, ids, cropped_boxes)
            inps = inps.to(self.device)
            hm = self.pose_model(inps)
            hm = hm.cpu()

            result = self.postprocess(
                boxes, scores, ids, hm, cropped_boxes, orig_img)
            # return inps, orig_img, boxes, scores, ids, cropped_boxes
            return result

    def postprocess(self, boxes, scores, ids, hm_data, cropped_boxes, orig_img):
        norm_type = self.cfg.LOSS.get('NORM_TYPE', None)
        hm_size = self.cfg.DATA_PRESET.HEATMAP_SIZE

        # get item
        (boxes, scores, ids, hm_data, cropped_boxes, orig_img)  # = self.item
        if orig_img is None:
            return None
        # image channel RGB->BGR
        orig_img = np.array(orig_img, dtype=np.uint8)[:, :, ::-1]
        self.orig_img = orig_img
        if boxes is None or len(boxes) == 0:
            return None
        else:
            # location prediction (n, kp, 2) | score prediction (n, kp, 1)
            assert hm_data.dim() == 4
            if hm_data.size()[1] == 136:
                self.eval_joints = [*range(0, 136)]
            elif hm_data.size()[1] == 26:
                self.eval_joints = [*range(0, 26)]
            pose_coords = []
            pose_scores = []

            for i in range(hm_data.shape[0]):
                bbox = cropped_boxes[i].tolist()
                pose_coord, pose_score = self.heatmap_to_coord(
                    hm_data[i][self.eval_joints], bbox, hm_shape=hm_size, norm_type=norm_type)
                pose_coords.append(torch.from_numpy(pose_coord).unsqueeze(0))
                pose_scores.append(torch.from_numpy(pose_score).unsqueeze(0))
            preds_img = torch.cat(pose_coords)
            preds_scores = torch.cat(pose_scores)

            boxes, scores, ids, preds_img, preds_scores, pick_ids = \
                pose_nms(boxes, scores, ids, preds_img,
                         preds_scores, 0)

            _result = []
            for k in range(len(scores)):
                _result.append(
                    {
                        'keypoints': preds_img[k],
                        'kp_score': preds_scores[k],
                        'proposal_score': torch.mean(preds_scores[k]) + scores[k] + 1.25 * max(preds_scores[k]),
                        'idx': ids[k],
                        'bbox': [boxes[k][0], boxes[k][1], boxes[k][2] - boxes[k][0], boxes[k][3] - boxes[k][1]]
                    }
                )
            result = {
                'result': _result
            }
        return result

    def vis(self, image, pose):
        if pose is not None:
            image = self.vis_frame(image, pose, self.opt)
        return image


def export_onnx(model_path, cfg_path):
    al = AlphaPose_v0(model_path=model_path, cfg_path=cfg_path)

    cfg = update_config("model-zoo/alpha_pose/256x192_res50_lr1e-3_1x.yaml")

    input_names = ['input']
    output_names = ['output']

    dummy_input = torch.randn(
        1, 3, *cfg.DATA_PRESET.IMAGE_SIZE, dtype=torch.float32).to('cuda:0')
    onnx_file_name = "alphaPose_{}_3_{}_{}_dynamic.onnx".format(
        1, *cfg.DATA_PRESET.IMAGE_SIZE)

    torch.onnx.export(al.pose_model, dummy_input, onnx_file_name, input_names=input_names,
                      output_names=output_names, verbose=True, opset_version=11)


if __name__ == "__main__":
    from m_yolo import *
    al = AlphaPose(engine_path="/home/kikai/Desktop/alphapose/model-zoo/fast_pose_res152/fast_421_res152_256x192.engine",
                # cfg_path="model-zoo/alpha_pose/256x192_res50_lr1e-3_1x.yaml")
                cfg_path="/home/kikai/Desktop/alphapose/model-zoo/fast_pose_res152/256x192_res152_lr1e-3_1x-duc.yaml")

    yolo = YoloV4(darknet="/home/kikai/Desktop/darknet/darknet",
                config_file="/home/kikai/Downloads/train/scaled_nobi_pose.cfg",
                data_file="/home/kikai/Downloads/train/scaled_nobi_pose.data",
                weights_file="/home/kikai/Downloads/scaled_nobi_pose_best.weights")
    c = 0
    # for path in ['samples/pose1.jpg', 'samples/pose2.jpg', 'samples/pose3.jpg', 'samples/pose4.jpg', 'samples/pose5.jpg', 'samples/pose6.jpg', 'samples/pose7.jpg', 'samples/pose8.jpg', 'samples/pose9.jpg', 'samples/pose10.jpg', 'samples/pose11.jpg', 'samples/pose12.jpg']:
    # for path in ["/home/kikai/Desktop/alphapose/3oxekd9mddiynxk52e6nh1jwaffn_0.jpg", "/home/kikai/Desktop/alphapose/3oxekd9mddiynxk52e6nh1jwaffn_2.jpg"]:
    path = "/home/kikai/Desktop/alphapose/3oxekd9mddiynxk52e6nh1jwaffn_0.jpg"
    image = cv2.imread(path)
    object_detected = yolo.process([image])
    boxes, scores = cvt_Yolo_format_to_Alpha(object_detected)
    pose = al.inference(image, boxes, scores)
    c +=1
    cv2.imwrite("test_"+ str(c) +".jpg", al.vis(image, pose))
        # cv2.imshow("HNIW oi", al.vis(image, pose))
        # cv2.waitKey(0)



